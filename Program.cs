﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;
using NUnit.Framework;
namespace selenium
{
    class Program
    {      
        public static bool IsElementDisplayed(IWebElement element) {
            return element.Displayed;
        }

        public static bool IsElementEnabled(IWebElement element) {
            return element.Enabled;
        }
        static void Main(string[] args) {
            IWebDriver driver = new ChromeDriver("./chromedriver.exe");
            // ChromeOptions options = new ChromeOptions();
            driver.Navigate().GoToUrl("https://parabank.parasoft.com/parabank/index.htm");
            IWebElement title = driver.FindElement(By.CssSelector("[title='ParaBank']"));
            IsElementDisplayed(title);
            Thread.Sleep(2000);
            driver.Quit();
        }
    }
}