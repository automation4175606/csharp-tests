using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using CSharpTests.Pages;

namespace CSharpTests.Tests {
    public class RegisterPageTests : BaseTest {

        [Test]
        public void RegisterNewUser() {
            HomePage hp = new HomePage(_driver);
            RegisterPage rp = new RegisterPage(_driver);
            _driver.Navigate().GoToUrl("https://parabank.parasoft.com/parabank/index.htm");
            hp.titleIsDisplayed();
            hp.clickRegisterButton();
            rp.FillFirstName();
            rp.FillLastName();
            rp.FillAddress();
            rp.FillCity();
            rp.FillState();
            rp.FillZipCode();
            rp.FillPhone();
            rp.FillSnn();
            rp.FillUsername();
            rp.FillPassword();
            rp.FillPasswordAgain();
            rp.clickRegisterButton();
            rp.checkRegisterStatus();
        }
    }
}
