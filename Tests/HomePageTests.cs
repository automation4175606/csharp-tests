using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using CSharpTests.Pages;

namespace CSharpTests.Tests {
    public class HomePageTests : BaseTest {

        [Test]
        public void IncorrectLoginToParabank() {
            HomePage hp = new HomePage(_driver);
            _driver.Navigate().GoToUrl("https://parabank.parasoft.com/parabank/index.htm");
            hp.titleIsDisplayed();
            hp.LoginToParabank("qwerty", "zxcvbn");
            hp.errorMessageHasProperlyText();
        }
    }
}
