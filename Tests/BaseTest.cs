using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace CSharpTests.Tests {
    public class BaseTest {
        protected IWebDriver _driver;

        [SetUp]
        public void initScript() {
           // System.Environment.SetEnvironmentVariable("webdriver.chrome.driver", @"D:\Projects\csharp-tests\chromedriver.exe");
            _driver = new ChromeDriver();
        }

        [TearDown]
        public void cleanUp() {
            _driver.Close();
            _driver.Quit();
        }   
    }
}