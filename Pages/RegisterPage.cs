using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using NUnit.Framework;

namespace CSharpTests.Pages {
    public class RegisterPage {
        private IWebDriver _driver;

        private String username;

        [FindsBy(How = How.CssSelector, Using = "[class='title']")]
        private IWebElement title;
        [FindsBy(How = How.Id, Using = "customer.firstName")]
        private IWebElement firstNameField;
        [FindsBy(How = How.Id, Using = "customer.lastName")]
        private IWebElement lastNameField;
        [FindsBy(How = How.Id, Using = "customer.address.street")]
        private IWebElement addressField;
        [FindsBy(How = How.Id, Using = "customer.address.city")]
        private IWebElement cityField;
        [FindsBy(How = How.Id, Using = "customer.address.state")]
        private IWebElement stateField;
        [FindsBy(How = How.Id, Using = "customer.address.zipCode")]
        private IWebElement zipCodeField;
        [FindsBy(How = How.Id, Using = "customer.phoneNumber")]
        private IWebElement phoneField;
        [FindsBy(How = How.Id, Using = "customer.ssn")]
        private IWebElement ssnField;
        [FindsBy(How = How.Id, Using = "customer.username")]
        private IWebElement userNameField;
        [FindsBy(How = How.Id, Using = "customer.password")]
        private IWebElement passwordField;
        [FindsBy(How = How.Id, Using = "repeatedPassword")]
        private IWebElement confirmField;
        [FindsBy(How = How.CssSelector, Using = "[value='Register']")]
        private IWebElement registerButton;
        [FindsBy(How = How.CssSelector, Using = "[class='title']")]
        private IWebElement registerStatus;

        //
        public RegisterPage(IWebDriver driver) {
            _driver = driver;
            PageFactory.InitElements(driver,this);
        }

        public String getRandomStringNumber() {
            Random rnd = new Random();
            int number = rnd.Next(1, 1000);
            return number.ToString();
        }

        public void FillFirstName() {
            firstNameField.SendKeys("first name");
        }

        public void FillLastName() {
            lastNameField.SendKeys("last name");
        }

        public void FillAddress() {
            addressField.SendKeys("address");
        }

        public void FillCity() {
            cityField.SendKeys("city");
        }

        public void FillState() {
            stateField.SendKeys("state");
        }

        public void FillZipCode() {
            zipCodeField.SendKeys("30-300");
        }

        public void FillPhone() {
            phoneField.SendKeys("888666999");
        }

        public void FillSnn() {
            ssnField.SendKeys("123456");
        }

        public void FillUsername() {
            username = "username" + getRandomStringNumber();
            userNameField.SendKeys(username);

        }

        public void FillPassword() {
            passwordField.SendKeys("Password123$");
        }

        public void FillPasswordAgain() {
            confirmField.SendKeys("Password123$");
        }

        public void clickRegisterButton() {
            registerButton.Click();
        }

        public void checkRegisterStatus() {
            Assert.AreEqual(registerStatus.Text, "Welcome " + username);
        }
    }
}

