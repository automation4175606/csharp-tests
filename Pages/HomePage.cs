using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace CSharpTests.Pages {
public class HomePage {
    private IWebDriver _driver;

    [FindsBy(How = How.CssSelector, Using = "[title='ParaBank']")]
    private IWebElement title;
    [FindsBy(How = How.Id, Using = "loginPanel")]
    private IWebElement loginPanel; 
    [FindsBy(How = How.Name, Using = "username")]
    private IWebElement userNameField;
    [FindsBy(How = How.Name, Using = "password")]
    private IWebElement passwordField;
    [FindsBy(How = How.CssSelector, Using = "[value='Log In']")]
    private IWebElement loginButton;
    [FindsBy(How = How.XPath, Using = "//*[text()='Register']")]
    private IWebElement registerButton;

    [FindsBy(How = How.CssSelector, Using = "[class='error']")]
    private IWebElement errorMessage;
    
    public HomePage(IWebDriver driver) {
        _driver = driver;
        PageFactory.InitElements(driver,this);
    }

    public bool titleIsDisplayed() {
        return title.Displayed;
    }

    public bool loginPanelIsDispalyed() {
        return loginPanel.Displayed;
    }

    public void LoginToParabank(String username, String password) {
        userNameField.SendKeys(username);
        passwordField.SendKeys(password);
        loginButton.Click();
    }

    public void clickRegisterButton() {
        registerButton.Click();
    }

    public void errorMessageHasProperlyText() {
        errorMessage.Text.Equals("The username and password could not be verified.");
    }
}
}

